import ecoles from "../data/EcoleSimplonOccitanie01.json";

function Info(props){
    return(
        <div>
            <h2>{ecoles[props.num].nom}</h2>
            <p>{ecoles[props.num].infos}</p>
            <p>{ecoles[props.num].formation}</p>
        </div>
    )
}

export default Info;