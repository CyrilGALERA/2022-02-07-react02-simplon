import '../styles/App.css';
import { MapContainer, TileLayer, Marker, Popup, PaneProps } from 'react-leaflet';
import Map_MarkerPC from './Map_MarkerPC';
import fabriques from '../data/EcoleSimplonOccitanie01.json';
import '../styles/styles.css';

import {useState} from "react";

function MapCyril(params) {

  // Geolocalisation
  const [posGeoL, setGeoL] = useState( [43.183766, 3.004212] );
  if ("geolocation" in navigator) {
    // /* la géolocalisation est disponible */
    navigator.geolocation.getCurrentPosition(function(posXY) {
      setGeoL( [posXY.coords.latitude, posXY.coords.longitude ] );
    })
  }

  // Meteo
  const [meteo, setMeteo] = useState();
  // meteo = {"coord":{"lon":139,"lat":35},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"base":"stations","main":{"temp":277.77,"feels_like":277.77,"temp_min":276.17,"temp_max":277.77,"pressure":1022,"humidity":79},"visibility":10000,"wind":{"speed":0.56,"deg":11,"gust":0.8},"clouds":{"all":95},"dt":1644578534,"sys":{"type":2,"id":2019346,"country":"JP","sunrise":1644528887,"sunset":1644567710},"timezone":32400,"id":1851632,"name":"Shuzenji","cod":200};
  function settingMeteo(lat,long) {
    // fetch('https://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + long + '&appid=45b95c3daba4851a9f5a9eca951a6eb9')
    fetch('https://api.openweathermap.org/data/2.5'
      +'/weather?lat='+lat+ '&lon='+long
      +'&units=metric&lang=fr'
      +'&appid=65d69a440af8877030e67f00933d1d70')
    .then(response => response.json())
    .then((promise) => {
      let jsonTab = promise;
      console.log(jsonTab);
      setMeteo(jsonTab);
    })
    .catch((err) => {console.log(err)});
  }

  return (
    <div>
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossOrigin=""/>
      <MapContainer center={[43.505, 2.35]} zoom={6} className="dy400px">
        
        <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {fabriques.map( (fabElt, fabNum) => {
          return(
          <Marker position={[fabElt.latitude, fabElt.longitude]}
              key={fabNum}
              eventHandlers={{
                  click: () => {
                    params.setInfo(fabNum);
                    settingMeteo(fabElt.latitude, fabElt.longitude);
                  },
              }}
          >
            <Popup>
                <h5>{meteo?.name} </h5>
                <div className="ligne axe2-center">
                  <img src={"http://openweathermap.org/img/w/" + meteo?.weather[0].icon + ".png"} alt="icone-Temps"></img>
                  <h5 style={{margin:0}} >{meteo?.main.temp} °C </h5>
                </div>
                <p style={{margin:0, marginBottom: 1 +"rem"}}>{meteo?.weather[0].description}</p>
                <div className="">
                  <p style={{margin:0}} >Humidité : {meteo?.main.humidity}</p>
                  <p style={{margin:0}} >Vent : {meteo?.wind.speed}</p>
                </div>
            </Popup>
          </Marker>)
        })}
        <Map_MarkerPC posPC={posGeoL}></Map_MarkerPC>
      </MapContainer>
    </div>
  );
}

export default MapCyril;