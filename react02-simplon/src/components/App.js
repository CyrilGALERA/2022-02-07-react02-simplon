// Styles
import '../styles/App.css';
// Sections
import Section00_header from './Section00_header';
import Section01 from './Section01';
import Section02_map from './Section02_map';
import Section03_footer from './Section03_footer';


function App() {
  // VARIABLE: const, let ...
  // FUNCTION: ...

  return(
    <div className="App">
      {/* // Header ================================================ */}
      <Section00_header></Section00_header>

      {/* // Section 1: ROUGE "Simplon.co en Occitanie" ============ */}
      <Section01> </Section01>

      {/* // Section 2: BLANCHE "Simplon.co prés de chez vous" ===== */}
      <Section02_map> </Section02_map>

      {/* // Footer ================================================ */}
      <Section03_footer></Section03_footer>
    </div>  
    );
}

export default App;