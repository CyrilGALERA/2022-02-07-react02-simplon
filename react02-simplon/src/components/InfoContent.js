import '../styles/styles.css';
import ecoles from "../data/EcoleSimplonOccitanie01.json";

function InfoContent(props){
    return(
        <div className="dx90 color-white bord-top" >
            {/* Reinit props: NON PAS POSSIBLE */}
            {/* {props.num = 0} */}
            {/* NON PAS POSSIBLE */}
            {/* Les infos dans props vont du père au fils et ne peuvent pas changer dans le fils */}
            <h5>{ecoles[props.num].nom}</h5>
            <p>{ecoles[props.num].infos}</p>
            <p className="padding-V10">{ecoles[props.num].formation}</p>
        </div>
    )
}

export default InfoContent;