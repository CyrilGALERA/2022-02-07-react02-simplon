import './App.css';
import fabriques from './ecolesSimplon01.json';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import {useState} from "react";
import Info from "./Info";


function MapMaxime() {
    const [i, setI] = useState(0);
    function changerIndex(index) {
        setI(index)
    }
  return (
    <div className="App">
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossOrigin=""/>
      <MapContainer center={[43.505, 2.35]} zoom={6} className="dy400px">
        <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
          {fabriques.map( (fab, index) => {
              return(
              <Marker position={[fab.latitude, fab.longitude]} key={index}
                      eventHandlers={{
                          click: () => {
                              changerIndex(index)
                          },
                      }}
              >
                  <Popup>
                      {fab.nom}
                  </Popup>
              </Marker>)
      })}
      </MapContainer>
        <Info num={i}></Info>
    </div>
  );
}

export default MapMaxime;