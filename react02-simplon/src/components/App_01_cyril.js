// App 01 : Composant Unique pour tous les paquets... => à sub-diviser !!! en sous composant
import '../styles/styles.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import ecolesSimplon from '../data/EcoleSimplonOccitanie01.json';
import { useState } from "react";

function App() {
  // Récupérer  - position actuel géolocalisé
  //            - position dans ecolesSimplon
  const position = [43.182062, 2.978459];
  const [data, setData] = useState();

  return (
    <div className="ligne center-elt dx90 ">
      {/* Col 1 Info ======================================== */}
      <div id="BoxInfoFather" className="dx40 col axe1-center axe2-center color-lightgray">
        
        <div id="BoxInfoContent" className="dx90 color-white bord-top">
            <h5 >{data ? data.nom : "Simplon Narbonne"}</h5>
            <p >{data ? data.infos : "30 Avenue Docteur Paul Pompidor, 11100 Narbonne, Occitanie, France"}</p>
            <p className='padding-V10'>{data ? data.formation : "Aucune formation en cours"}</p>
        </div>

      </div>

      {/* Col 2 Map ========================================= */}
      <div className="dx60 "> 

        <MapContainer center={position} zoom={13} className="dy400px">

          {/* Insertion Map leaflet ================== */}
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />

          {/* MARKER : Position local actuelle géolocalisé ================== */}
          <Marker position={position}>
            <Popup>
              Bonjour <br /> Vous êtes ici.
            </Popup>
          </Marker>

          {/* MARKER : Position des diff ecolesSimplon JSON ================== */}
          {ecolesSimplon.map((ecole) => (
            <Marker
              position={[ecole.latitude, ecole.longitude]}
              
              eventHandlers={{
                click: () => {
                  setData(ecole);
                },
              }}
            >
            
              <Popup>
                <div id={ecole.nom}>
                  <h5> {ecole.nom} </h5>
                  <p> {ecole.infos} </p>
                </div>
              </Popup>

            </Marker>
          ))}

        </MapContainer>
        
      </div>
    </div>
  )
}

export default App;
