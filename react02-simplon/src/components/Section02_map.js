import '../styles/styles.css';

import Text_simplon from './Text_simplon';
import InfoContent from "./InfoContent";
import MapCyril from './MapCyril';

import {useState} from "react";

function Section02_map() {
  // index : dans la liste des ecoles Simplon JSON
  const [index, setIndex] = useState(0); 
  function settingIndex(index) {
    setIndex(index)
  }
  // pas la peine: utiliser direct setIndex dans ce cas la...
  //              <MapCyril setInfo={settingIndex}> </MapCyril>
  //              <MapCyril setInfo={setIndex}> </MapCyril>

  return (
    <div>
      <Text_simplon></Text_simplon>

      <div className="ligne center-elt dx90 ">
      {/* Div Info + Map ============= */}

        <div id="BoxInfoFather" className="dx40 col axe1-center axe2-center color-lightgray">
          <InfoContent num={index}></InfoContent>
        </div>
        <div className="dx60"> 
          <MapCyril setInfo={settingIndex}> </MapCyril>
        </div>
      </div>

    </div>
  )
}

export default Section02_map;