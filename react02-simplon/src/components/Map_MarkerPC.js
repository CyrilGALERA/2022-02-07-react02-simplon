import '../styles/styles.css';
import { Marker, Popup } from 'react-leaflet';
import MarkerRed from './MarkerRed';

function Map_MarkerPC(params) {
  console.log("Map_MarkerPC");
  console.log("params.posPC = ", params.posPC);
  return (
    <div>
      <Marker position={params.posPC} key={0} icon={MarkerRed}>
        <Popup> Vous êtes ici ! </Popup>
      </Marker>
    </div>
  )
}

export default Map_MarkerPC;