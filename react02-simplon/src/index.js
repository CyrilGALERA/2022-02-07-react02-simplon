import React from 'react';
import ReactDOM from 'react-dom';
import '../src/styles/index.css';
import App from './components/App';
import App_02_max from './components/App_02_max';
import Text_simplon from './components/Text_simplon';
import MapSimplon from './components/App';
// import PopupExample from './components/PopupExample';

import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
    {/* <Text_simplon /> */}
    {/* <MapSimplon /> */}
    {/* <PopupExample /> */}
    {/* NON !!! Tout cela dans App ! ICI ne laisser juste App  !!!!!!!! */}
  </React.StrictMode>,
  document.getElementById('titre-map')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
